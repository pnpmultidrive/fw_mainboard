/*
 * gcode.c
 *
 *  Created on: Jan 14, 2023
 *      Author: kamilo
 */

#include "lib/gcode.h"
#include "lib/timer.h"

void GCode_Init (gcode_control_t *gcp, serial_control_t *scp, stepper_control_t *stcp, timer_control_t *tcp)
{
    gcp->scp = scp;
    gcp->stcp = stcp;
    gcp->tcp = tcp;

    gcp->pop = 0;
    gcp->push = 0;
    gcp->use = 0;

    //gcp->state = GCODE_STATE_AVAILABLE;
    gcp->state = GCODE_STATE_TESTTX;
    Timer_Start(tcp,TIMER_GCODE, 1000, TIMER_CONTINUOUS);
}

void GCode_Task (gcode_control_t *gcp)
{
    switch(gcp->state)
    {
    case GCODE_STATE_AVAILABLE:
        if(gcp->use > 0)
        {
            if(Stepper_Available(gcp->stcp) == 0)
            {
                Stepper_Move(gcp->stcp, &gcp->buf[gcp->pop]);

                gcp->pop++;
                if(gcp->pop == GCODE_MOVE_BUFFER_LENGTH) gcp->pop = 0;
                gcp->use--;

                gcp->state = GCODE_STATE_RUNNING;
            }
        }
        break;
    case GCODE_STATE_RUNNING:
        if(Stepper_Available(gcp->stcp) == 0)
        {
            gcp->state = GCODE_STATE_AVAILABLE;
            Serial_Print(gcp->scp, "ok\n", 3);
            Stepper_DisableMotors (gcp->stcp);
        }
        break;
    case GCODE_STATE_HOMING:
        if(Stepper_LimitFlags(gcp->stcp, STEPPER_B_X_LIMIT | STEPPER_B_Y_LIMIT) == 0)
        {
            gcp->state = GCODE_STATE_AVAILABLE;
            Serial_Print(gcp->scp, "Homing ok\n", 10);
            Stepper_DisableMotors (gcp->stcp);
            //todo: all position to zero
        }
        else if(Stepper_Available(gcp->stcp) == 0)
        {
            gcp->state = GCODE_STATE_AVAILABLE;
            Serial_Print(gcp->scp, "Homing fail\n", 12);
            Stepper_DisableMotors (gcp->stcp);
        }
        break;
    case GCODE_STATE_TESTTX:
        if(Timer_Available(gcp->tcp, TIMER_GCODE))
        {
            __no_operation();
            Serial_Print(gcp->scp, "Test\n", 5);
        }
        break;
    default:
        break;
    }
}


unsigned int GCode_Decoder_GNum (unsigned char *buf)
{
    //TODO: Add hysteresis compensation
    unsigned char *p;
    unsigned int i, sum;
    p = buf;
    if(*p != 'G')
        return 999;
    p++;

    while((*p >= '0') && (*p <= '9')) p++;

    i = 1;
    sum = 0;
    p--;
    while(*p != 'G')
    {
        sum += ((*p)&0x0F)*i;
        p--;
        i *= 10;
    }

    return sum;
}

unsigned char GCode_Decoder_G1 (gcode_control_t *gcp, unsigned char *buf)
{
    unsigned char *p,*p_aux, *p_dot, charEnd;
    long aux, *xyz_aux;
    stepper_move_t xyzf;

    xyzf.x = 0;
    xyzf.y = 0;
    xyzf.f = 0;


    p = buf;
    while((*p != 0x0D) && (*p != 0x0A) && (*p != ';'))//Finished string with LF, CR or ;
    {
        while((*p != 'X') && (*p != 'Y') && (*p != 'Z') && (*p != 'F') && (*p != 0x0A) && (*p != 0x0D)) p++;
        if((*p == 0x0D) || (*p == 0x0A)) break;
        switch(*p)
        {
        case 'X':
            xyz_aux = &xyzf.x;
            break;
        case 'Y':
            xyz_aux = &xyzf.y;
            break;
        case 'Z':
            xyz_aux = &xyzf.z;
            break;
        case 'F':
            xyz_aux = &xyzf.f;
            break;
        default:
            break;
        }
        charEnd = *p;
        p_aux = p;
        p_aux++;
        while((*p_aux != '.') &&
              (*p_aux != 0x0A) &&
              (*p_aux != 0x0D) &&
              (*p_aux != ' ') &&
              (*p_aux != 'X') &&
              (*p_aux != 'Y') &&
              (*p_aux != 'Z') &&
              (*p_aux != ';') &&
              (*p_aux != 'F')) p_aux++;
        if(*p_aux == '.')
        {
            p_dot = p_aux;
            p_dot++;
            if((*p_dot <= '9') && (*p_dot >= '0'))
            {
                *xyz_aux += (*p_dot&0x0F)*100;
                p_dot++;
                if((*p_dot <= '9') && (*p_dot >= '0'))
                {
                    *xyz_aux += (*p_dot&0x0F)*10;
                    p_dot++;
                    if((*p_dot <= '9') && (*p_dot >= '0'))
                        *xyz_aux += (*p_dot&0x0F);
                }
            }
        }

        p_aux--;
        if(charEnd == 'F') aux = 1;
        else aux = 1000;
        while((*p_aux != charEnd) && (*p_aux != '-'))
        {
            *xyz_aux += ((*p_aux)&0x0F)*aux;
            aux *= 10;
            p_aux--;
        }
        p++;
        if(*p == '-') *xyz_aux *= -1;
    }

    if((xyzf.x == 0) && (xyzf.y == 0) && (xyzf.z == 0))
        return 1;
    if(gcp->use == GCODE_MOVE_BUFFER_LENGTH)
        return 2;

    Stepper_Convert(gcp->stcp, &gcp->buf[gcp->push], xyzf);

    gcp->push++;
    if(gcp->push == GCODE_MOVE_BUFFER_LENGTH) gcp->push = 0;
    gcp->use++;

    return 0;
}

void GCode_M0(gcode_control_t *gcp)
{
    gcp->state = GCODE_STATE_AVAILABLE;
    Stepper_StopMotors (gcp->stcp);
    Stepper_DisableMotors(gcp->stcp);
}

void GCode_Homing (gcode_control_t *gcp)
{
    stepper_move_t xyzf;

    xyzf.x = GCODE_X_MAX_LENGHT;
    xyzf.y = GCODE_Y_MAX_LENGHT;
    xyzf.z = GCODE_Z_MAX_LENGHT;
    xyzf.f = GCODE_F_MAX_LENGHT;

    Stepper_Convert(gcp->stcp, &gcp->buf[gcp->push], xyzf);
    gcp->push++;
    if(gcp->push == GCODE_MOVE_BUFFER_LENGTH) gcp->push = 0;
    gcp->use++;
}
