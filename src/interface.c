/*
 * interface.c
 *
 *  Created on: Jan 13, 2023
 *      Author: kamilo
 */

#include "lib/interface.h"
#include "lib/gcode.h"
#include "lib/rs485.h"
#include "config.h"

void Interface_Init (interface_control_t *icp, serial_control_t *scp, gcode_control_t *gcp, rs485_control_t *rcp)
{
    icp->scp = scp;
    icp->gcp = gcp;
    icp->rcp = rcp;
}

void Interface_Task (interface_control_t *icp)
{
    unsigned int available;
    unsigned char buf[64], response;
    unsigned int aux;

    //gcode_move_t xyzf;

    available = Serial_Available(icp->scp);
    if(available > 0)
    {
        Serial_Read(icp->scp, &buf[0], available);

        if(buf[0] == 'V')   //Version
        {
            Serial_Print(icp->scp, FW_VERSION, FW_VERSION_LENGHT);
            Serial_Print(icp->scp, "\n", 1);
        }
        else if(buf[0] == '$') //System commands
        {

        }
        else if(buf[0] == 'G') //G commands
        {
            aux = GCode_Decoder_GNum (&buf[0]);
            __no_operation();
            switch(aux)
            {
            case 1://Moved command
                response = GCode_Decoder_G1(icp->gcp, &buf[0]);
                if(response != 0)
                    Serial_Print(icp->scp, "err\n", 4);
                break;
            default:
                break;
            }
        }
        else if(buf[0] == 'h') //Header commands
        {
            RS485_Print(icp->rcp,&buf[0],available);
        }
        else if((buf[0] == 'M') && (buf[1] == '0'))
        {
            GCode_M0(icp->gcp);
            Serial_Print(icp->scp, "stop\n", 5);
        }
    }
    available = RS485_Available(icp->rcp);
    if(available > 0)
    {
        RS485_Read(icp->rcp, &buf[0]);
        if(buf[0] == 'r')
        {
            Serial_Print(icp->scp, &buf[0], available);
        }
    }
}


