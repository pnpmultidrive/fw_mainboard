/*
 * serial.c
 *
 *  Created on: 25 ago. 2022
 *      Author: kamilo
 */

#include <msp430.h>
#include "lib/serial.h"
#include "config.h"

//----------------------------------------------------------------------------
//---------------------------------- General ---------------------------------
//----------------------------------------------------------------------------
void Serial_Init (serial_control_t *scp)
{
    scp->counter = 0;
    scp->rx_pop = 0;
    scp->rx_push = 0;
    scp->rx_use = 0;
    scp->tx_pop = 0;
    scp->tx_push = 0;
    scp->tx_use = 0;

    P1SEL0 |= BIT4 | BIT5;
    UCA0CTLW0 |= UCSWRST;
    UCA0CTLW0 |= UCSSEL__SMCLK;
    UCA0BR0 = 0x45;
    UCA0BR1 = 0x00;
    UCA0MCTLW = 0xAA00;
    UCA0CTLW0 &= ~UCSWRST;
    UCA0IE |= UCRXIE;
    //115200 bauds
};

void Serial_Task (serial_control_t *scp)
{
    unsigned char data;
    if(scp->aux_use > 0)
    {
        __disable_interrupt();
        scp->aux_use--;
        data = scp->aux_buffer[scp->aux_pop];
        __enable_interrupt();
        scp->aux_pop++;
        if(scp->aux_pop == SERIAL_AUX_BUFFER_SIZE) scp->aux_pop = 0;

        scp->rx_buffer[scp->rx_push] = data;
        scp->rx_push++;
        if(scp->rx_push == SERIAL_RX_BUFFER_SIZE) scp->rx_push = 0;
        scp->counter++;
#ifdef SERIAL0_ECO_MODE
        Serial_Print(scp,&data,1);
        if(data == 0x0D)
            Serial_Print(scp,"\n",1);
#endif

        if((data == 0x0A) || (data == 0x0D))
        {
            scp->rx_use += scp->counter;
            scp->counter = 0;
        }
    }
};

//----------------------------------------------------------------------------
//----------------------------- Interfaces serial ----------------------------
//----------------------------------------------------------------------------

void Serial_Print(serial_control_t *scp,
                   unsigned char *bufer,
                   unsigned char length)
{
    unsigned char i;
    for(i = 0; i < length; i++)
    {
        if(scp->tx_use < SERIAL_TX_BUFFER_SIZE)
        {
            scp->tx_buffer[scp->tx_push] = *(bufer + i);
            scp->tx_push++;
            if(scp->tx_push == SERIAL_TX_BUFFER_SIZE) scp->tx_push = 0;
            scp->tx_use++;
        }
    }
    UCA0IE |= UCTXIE;
}

unsigned int Serial_Available(serial_control_t *scp)
{
    return scp->rx_use;
}

void Serial_Read(serial_control_t *scp,
               unsigned char *buffer,
               unsigned char length)
{
    unsigned char i;
    for(i = 0; i < length; i++)
    {
        *(buffer + i) = scp->rx_buffer[scp->rx_pop];
        scp->rx_pop++;
        if(scp->rx_pop == SERIAL_RX_BUFFER_SIZE) scp->rx_pop = 0;
        scp->rx_use--;
    }
}

//---------------------------------- UART 0 --------------------------------------
#pragma vector=USCI_A0_VECTOR
__interrupt void USCI_A0_ISR(void)
{
    extern serial_control_t scp;

    switch(__even_in_range(UCA0IV,USCI_UART_UCTXCPTIFG))
    {
      case USCI_NONE:
          break;
      case USCI_UART_UCRXIFG:
          scp.aux_buffer[scp.aux_push] = UCA0RXBUF;
          scp.aux_push++;
          if(scp.aux_push == SERIAL_AUX_BUFFER_SIZE) scp.aux_push = 0;
          scp.aux_use++;
          break;
      case USCI_UART_UCTXIFG:
          UCA0TXBUF = scp.tx_buffer[scp.tx_pop];
          scp.tx_pop++;
          if(scp.tx_pop == SERIAL_TX_BUFFER_SIZE) scp.tx_pop = 0;
          scp.tx_use--;
          if(scp.tx_use == 0)
               UCA0IE &= ~UCTXIE;
          break;
      case USCI_UART_UCSTTIFG:
          break;
      case USCI_UART_UCTXCPTIFG:
          break;
      default:
          break;
  }
}
