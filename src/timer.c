/*
 * timer.c
 *
 *  Created on: 25 ago. 2022
 *      Author: kamilo
 */

#include <msp430.h>
#include "lib/timer.h"

void Timer_Init (timer_control_t *tcp)
{
    char i;

    TA0CCR0 = 8000;
    TA0CTL = TASSEL__SMCLK | MC__UP;             // SMCLK, UP mode
    TA0CCTL0 |= CCIE;                             // TACCR0 interrupt enabled

    for(i = 0; i > TIMER_SIZE; i++)
    {
        tcp->timer[i].count = 0;
        tcp->timer[i].flags = 0;
        tcp->timer[i].limit = 0;
    }
}


void Timer_Task (timer_control_t *tcp)
{
    char i;
    if(tcp->tick>0)
    {
        tcp->tick--;
        i = TIMER_SIZE;
        while(i>0)
        {
            i--;
            if((tcp->timer[i].flags&TIMER_B_ENABLE) != 0)
            {
                tcp->timer[i].count++;
                if(tcp->timer[i].count == tcp->timer[i].limit)
                {
                    tcp->timer[i].flags |= TIMER_B_FINISH;
                    tcp->timer[i].count = 0;
                    if((tcp->timer[i].flags&TIMER_B_CONTINOUS) == 0)
                        tcp->timer[i].flags &= ~TIMER_B_ENABLE;
                }
            }
        }
    }
}

#pragma vector = TIMER0_A0_VECTOR
__interrupt void Timer_A (void)
{
    extern timer_control_t tcp;
    tcp.tick++;
}


void Timer_Start(timer_control_t *tcp, unsigned char id, unsigned int time, unsigned char continuous)
{
    tcp->timer[id].limit = time;
    tcp->timer[id].flags = TIMER_B_ENABLE;
    tcp->timer[id].count = 0;
    if(continuous != 0)
        tcp->timer[id].flags |= TIMER_B_CONTINOUS;
    else
        tcp->timer[id].flags &= ~TIMER_B_CONTINOUS;
}

unsigned char Timer_Available(timer_control_t *tcp, unsigned char id)
{
    if((tcp->timer[id].flags&TIMER_B_FINISH) == 0)
        return 0;
    tcp->timer[id].flags &= ~TIMER_B_FINISH;
    return 1;
}

void Timer_Stop(timer_control_t *tcp, unsigned char id)
{
    tcp->timer[id].flags = 0;
    tcp->timer[id].count = 0;
}


