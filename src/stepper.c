/*
 * stepper.c
 *
 *  Created on: Jan 14, 2023
 *      Author: kamilo
 */

#include <msp430.h>
#include "lib/stepper.h"

void Stepper_Init (stepper_control_t *stcp)
{
    stepper_storage_t ss;
    unsigned int *Flash_ptr,i,*p;

    TA1CTL = TASSEL__SMCLK | ID__8 | MC__UP;             // SMCLK, UP mode
    TA2CTL = TASSEL__SMCLK | ID__8 | MC__UP;


    stcp->x_location = 0;
    stcp->x_step_count = 0;

    stcp->y_location = 0;
    stcp->y_step_count = 0;


    STEPPER_X_PIN_EN_DIR |= STEPPER_X_PIN_EN;
    STEPPER_X_PIN_DIR_DIR |= STEPPER_X_PIN_DIR;
    STEPPER_X_PIN_STEP_DIR |= STEPPER_X_PIN_STEP;

    STEPPER_Y_PIN_EN_DIR |= STEPPER_Y_PIN_EN;
    STEPPER_Y_PIN_DIR_DIR |= STEPPER_Y_PIN_DIR;
    STEPPER_Y_PIN_STEP_DIR |= STEPPER_Y_PIN_STEP;

    STEPPER_X_PIN_EN_OUT |= STEPPER_X_PIN_EN;//Disable X Motor
    STEPPER_Y_PIN_EN_OUT |= STEPPER_Y_PIN_EN;//Disable Y Motor

    STEPPER_X_LIMIT_DIR &= ~STEPPER_X_LIMIT_PIN;
    STEPPER_Y_LIMIT_DIR &= ~STEPPER_Y_LIMIT_PIN;

    Flash_ptr = (unsigned int *) 0x1800;              // Initialize Flash pointer
    p = (unsigned int*)(&ss);
    for(i = 0; i < 4; i++)
        *p++ = *Flash_ptr++;

    //--------- Stepper per mm (X) --------------
    if(ss.x_step_mm == 0xffffffff)
        stcp->x_step_mm = STEPPER_X_STEPPERXMM;
    else
        stcp->x_step_mm = ss.x_step_mm;

    //--------- Stepper per mm (Y) --------------
    if(ss.y_step_mm == 0xffffffff)
        stcp->y_step_mm = STEPPER_Y_STEPPERXMM;
    else
        stcp->y_step_mm = ss.y_step_mm;


}


#pragma vector = TIMER1_A0_VECTOR
__interrupt void Timer1 (void)
{
    extern stepper_control_t stcp;
    STEPPER_X_PIN_STEP_OUT |= STEPPER_X_PIN_STEP;
    stcp.x_step_count--;
    if(stcp.x_step_count == 0)
    {
        TA1CCTL0 &=~CCIE;
        stcp.flags &= ~STEPPER_B_X_BUSSY;
    }
    if(STEPPER_X_LIMIT_IN&STEPPER_X_LIMIT_PIN == 0)
    {
        TA1CCTL0 &=~CCIE;
        stcp.flags &= ~STEPPER_B_X_BUSSY;
        stcp.flags |= STEPPER_B_X_LIMIT;
        stcp.x_step_count = 0;
    }
    STEPPER_X_PIN_STEP_OUT &= ~STEPPER_X_PIN_STEP;
}

#pragma vector = TIMER2_A0_VECTOR
__interrupt void Timer2 (void)
{
    extern stepper_control_t stcp;
    STEPPER_Y_PIN_STEP_OUT |= STEPPER_Y_PIN_STEP;
    stcp.y_step_count--;
    if(stcp.y_step_count == 0)
    {
        TA2CCTL0 &=~CCIE;
        stcp.flags &= ~STEPPER_B_Y_BUSSY;
    }
    if(STEPPER_Y_LIMIT_IN&STEPPER_Y_LIMIT_PIN == 0)
    {
        TA2CCTL0 &=~CCIE;
        stcp.flags &= ~STEPPER_B_Y_BUSSY;
        stcp.flags |= STEPPER_B_Y_LIMIT;
        stcp.y_step_count = 0;
    }
    STEPPER_Y_PIN_STEP_OUT &= ~STEPPER_Y_PIN_STEP;
}


void Stepper_Convert(stepper_control_t *stcp, stepper_buffer_t *xyz, stepper_move_t move)
{
    long x_abs, y_abs, z_abs, max;
    long f;

    //---------- Convert negative in positive ---------
    x_abs = abs(move.x);
    y_abs = abs(move.y);
    z_abs = abs(move.z);

    //----------------- detect max number ---------------
    max = x_abs;
    if(y_abs > max) max = y_abs;
    if(z_abs > max) max = z_abs;

    //------------- Calculate relative feed -------------
    if(move.x != 0)
    {
        f = (move.f*x_abs)/max;
        f = ((STEPPER_FEED)/(f*stcp->x_step_mm));
        if(f < STEPPER_MAX_FEED_X) f = STEPPER_MAX_FEED_X;
        if(f > 65535.0) f = 65535.0;
        xyz->x_feed = f;
        xyz->x = x_abs*stcp->x_step_mm/1000;
        if(move.x > 0)
            xyz->x_dir = 0;
        else
            xyz->x_dir = 1;
    }
    else
    {
        xyz->x = 0;
        xyz->x_dir = 0;
        xyz->x_feed = 0;
    }
    if(move.y != 0)
    {
        f = (move.f*y_abs)/max;
        f = ((STEPPER_FEED)/(f*stcp->y_step_mm));
        if(f < STEPPER_MAX_FEED_Y) f = STEPPER_MAX_FEED_Y;
        if(f > 65535) f = 65535;
        xyz->y_feed = f;
        xyz->y = y_abs*stcp->y_step_mm/1000;
        if(move.y > 0)
            xyz->y_dir = 0;
        else
            xyz->y_dir = 1;
    }
    else
    {
        xyz->y = 0;
        xyz->y_dir = 0;
        xyz->y_feed = 0;
    }
}

unsigned char Stepper_Available(stepper_control_t *stcp)
{
    if(stcp->flags != 0)
        return 1;
    return 0;
}

void Stepper_DisableMotors (stepper_control_t *stcp)
{
    STEPPER_X_PIN_EN_OUT |= STEPPER_X_PIN_EN;//Disable X Motor
    STEPPER_Y_PIN_EN_OUT |= STEPPER_Y_PIN_EN;//Disable Y Motor

}

void Stepper_Move(stepper_control_t *stcp, stepper_buffer_t *buf)
{
    //------------- Calculate relative feed -------------
    if(buf->x != 0)
    {
        if(buf->x_dir == 0)
            STEPPER_X_PIN_DIR_OUT |= STEPPER_X_PIN_DIR;
        else
            STEPPER_X_PIN_DIR_OUT &= ~STEPPER_X_PIN_DIR;
        stcp->flags |= STEPPER_B_X_BUSSY;
        TA1CCR0 = buf->x_feed;
        stcp->x_step_count = buf->x;
        if((STEPPER_X_LIMIT_IN&STEPPER_X_LIMIT_PIN == 0) &&
           (buf->x_dir != 0))
            stcp->x_step_count = 0;
    }
    if(buf->y != 0)
    {
        if(buf->y_dir == 0)
            STEPPER_Y_PIN_DIR_OUT |= STEPPER_Y_PIN_DIR;
        else
            STEPPER_Y_PIN_DIR_OUT &= ~STEPPER_Y_PIN_DIR;
        stcp->flags |= STEPPER_B_Y_BUSSY;
        TA2CCR0 = buf->y_feed;
        stcp->y_step_count = buf->y;
        if((STEPPER_Y_LIMIT_IN&STEPPER_Y_LIMIT_PIN == 0) &&
           (buf->y_dir != 0))
            stcp->y_step_count = 0;
    }

    //---------------- Init hardware control ---------------
    if(stcp->x_step_count > 0)
    {
        STEPPER_X_PIN_EN_OUT &= ~STEPPER_X_PIN_EN;
        TA1CCTL0 |= CCIE;
    }

    if(stcp->y_step_count > 0)
    {
        STEPPER_Y_PIN_EN_OUT &= ~STEPPER_Y_PIN_EN;
        TA2CCTL0 |= CCIE;
    }
}

void Stepper_StopMotors (stepper_control_t *stcp)
{
    TA1CCTL0 &=~CCIE;
    TA2CCTL0 &=~CCIE;
    TA3CCTL0 &=~CCIE;
    stcp->flags &= ~STEPPER_B_X_BUSSY;
    stcp->flags &= ~STEPPER_B_Y_BUSSY;
    stcp->flags &= ~STEPPER_B_Z_BUSSY;
    stcp->x_step_count = 0;
    stcp->y_step_count = 0;

}

unsigned char Stepper_LimitFlags (stepper_control_t *stcp, unsigned char mask)
{
    return (stcp->flags&mask);
}
