/*
 * Interface.h
 *
 *  Created on: Jan 13, 2023
 *      Author: kamilo
 */

#ifndef LIB_INTERFACE_H_
#define LIB_INTERFACE_H_

#include "lib/serial.h"
#include "lib/gcode.h"
#include "lib/rs485.h"

typedef struct interface_control_t interface_control_t;
struct interface_control_t
{
    serial_control_t *scp;
    gcode_control_t *gcp;
    rs485_control_t *rcp;
};

void Interface_Init (interface_control_t *icp, serial_control_t *scp, gcode_control_t *gcp, rs485_control_t *rcp);
void Interface_Task (interface_control_t *icp);


#endif /* LIB_INTERFACE_H_ */
