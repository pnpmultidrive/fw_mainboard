/*
 * serial.h
 *
 *  Created on: 25 ago. 2022
 *      Author: kamilo
 */

#ifndef LIB_SERIAL_H_
#define LIB_SERIAL_H_

//----------------------------------------------------------------------------
//--------------------------------- Defines ----------------------------------
//----------------------------------------------------------------------------
#define SERIAL_RX_BUFFER_SIZE      64
#define SERIAL_TX_BUFFER_SIZE      64
#define SERIAL_AUX_BUFFER_SIZE     16

//----------------------------------------------------------------------------
//--------------------------------- Structure --------------------------------
//----------------------------------------------------------------------------

typedef struct serial_control_t serial_control_t;
struct serial_control_t
{
    unsigned char counter;

    unsigned char tx_push;
    unsigned char tx_pop;
    unsigned char tx_use;
    char tx_buffer[SERIAL_TX_BUFFER_SIZE];

    unsigned char rx_push;
    unsigned char rx_pop;
    unsigned char rx_use;
    char rx_buffer[SERIAL_RX_BUFFER_SIZE];

    unsigned char aux_push;
    unsigned char aux_pop;
    unsigned char aux_use;
    char aux_buffer[SERIAL_AUX_BUFFER_SIZE];
};

//----------------------------------------------------------------------------
//---------------------------------- General ---------------------------------
//----------------------------------------------------------------------------
void Serial_Init (serial_control_t *scp);
void Serial_Task (serial_control_t *scp);

//----------------------------------------------------------------------------
//-------------------------------- Interfaces --------------------------------
//----------------------------------------------------------------------------

void Serial_Print(serial_control_t *scp,
                   unsigned char *bufer,
                   unsigned char length);


unsigned int Serial_Available(serial_control_t *scp);

void Serial_Read(serial_control_t *scp,
               unsigned char *buffer,
               unsigned char length);

#endif /* LIB_SERIAL_H_ */
