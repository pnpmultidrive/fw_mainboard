/*
 * gcode.h
 *
 *  Created on: Jan 14, 2023
 *      Author: kamilo
 */

#ifndef LIB_GCODE_H_
#define LIB_GCODE_H_

#include "lib/serial.h"
#include "lib/stepper.h"
#include "lib/timer.h"

//----------------------------------------------------------------------------
//--------------------------------- Defines ----------------------------------
//----------------------------------------------------------------------------
#define GCODE_STATE_AVAILABLE       1
#define GCODE_STATE_RUNNING         2
#define GCODE_STATE_HOMING          3
#define GCODE_STATE_TESTTX          4

#define GCODE_MOVE_BUFFER_LENGTH    4

#define GCODE_X_MAX_LENGHT          40000//40 cm
#define GCODE_Y_MAX_LENGHT          30000//30 cm
#define GCODE_Z_MAX_LENGHT          10000//10 cm
#define GCODE_F_MAX_LENGHT          5000//5000 mmxmin

//----------------------------------------------------------------------------
//--------------------------------- Structure --------------------------------
//----------------------------------------------------------------------------
typedef struct gcode_move_t gcode_move_t;
struct gcode_move_t
{
    long x;
    long y;
    long z;
    long f;
};


typedef struct gcode_control_t gcode_control_t;
struct gcode_control_t
{
    serial_control_t *scp;
    stepper_control_t *stcp;
    timer_control_t *tcp;

    stepper_buffer_t buf[GCODE_MOVE_BUFFER_LENGTH];
    unsigned char use;
    unsigned char push;
    unsigned char pop;

    unsigned char state;
};

//----------------------------------------------------------------------------
//---------------------------------- General ---------------------------------
//----------------------------------------------------------------------------
void GCode_Init (gcode_control_t *gcp, serial_control_t *scp, stepper_control_t *stcp, timer_control_t *tcp);
void GCode_Task (gcode_control_t *gcp);

//----------------------------------------------------------------------------
//-------------------------------- Interfaces --------------------------------
//----------------------------------------------------------------------------

unsigned char GCode_BufferAvailable (gcode_control_t *gcp);
unsigned int GCode_Decoder_GNum (unsigned char *buf);
unsigned char GCode_Decoder_G1 (gcode_control_t *gcp, unsigned char *buf);
void GCode_M0(gcode_control_t *gcp);
void GCode_Homing (gcode_control_t *gcp);

#endif /* LIB_GCODE_H_ */
