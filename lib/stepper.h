/*
 * stepper.h
 *
 *  Created on: Jan 14, 2023
 *      Author: kamilo
 */

#ifndef LIB_STEPPER_H_
#define LIB_STEPPER_H_

//----------------------------------------------------------------------------
//--------------------------------- Defines ----------------------------------
//----------------------------------------------------------------------------

#define STEPPER_B_X_BUSSY       0x01
#define STEPPER_B_Y_BUSSY       0x02
#define STEPPER_B_Z_BUSSY       0x04

#define STEPPER_B_X_LIMIT       0x10
#define STEPPER_B_Y_LIMIT       0x20
#define STEPPER_B_Z_LIMIT       0x40

#define STEPPER_CLOCK           1000000
#define STEPPER_FEED            STEPPER_CLOCK*60
#define STEPPER_DEFAULT_FEED    2000//mm/min


#define STEPPER_MAX_FEED_X      38
#define STEPPER_MAX_FEED_Y      38
#define STEPPER_MAX_FEED_Z      2500

//----------------- X Driver -----------------
#define STEPPER_X_PIN_DIR       BIT1
#define STEPPER_X_PIN_DIR_OUT   P3OUT
#define STEPPER_X_PIN_DIR_DIR   P3DIR

#define STEPPER_X_PIN_STEP      BIT3
#define STEPPER_X_PIN_STEP_OUT  P2OUT
#define STEPPER_X_PIN_STEP_DIR  P2DIR

#define STEPPER_X_PIN_EN        BIT0
#define STEPPER_X_PIN_EN_OUT    P3OUT
#define STEPPER_X_PIN_EN_DIR    P3DIR

#define STEPPER_X_STEPPERXMM    100

//----------------- Y Driver -----------------
#define STEPPER_Y_PIN_DIR       BIT2
#define STEPPER_Y_PIN_DIR_OUT   P2OUT
#define STEPPER_Y_PIN_DIR_DIR   P2DIR

#define STEPPER_Y_PIN_STEP      BIT3
#define STEPPER_Y_PIN_STEP_OUT  P1OUT
#define STEPPER_Y_PIN_STEP_DIR  P1DIR

#define STEPPER_Y_PIN_EN        BIT2
#define STEPPER_Y_PIN_EN_OUT    P1OUT
#define STEPPER_Y_PIN_EN_DIR    P1DIR

#define STEPPER_Y_STEPPERXMM    100


//----------------- Limits -----------------
#define STEPPER_X_LIMIT_PIN           BIT6
#define STEPPER_X_LIMIT_DIR           P1DIR
#define STEPPER_X_LIMIT_OUT           P1OUT
#define STEPPER_X_LIMIT_IN            P1IN

#define STEPPER_Y_LIMIT_PIN           BIT7
#define STEPPER_Y_LIMIT_DIR           P1DIR
#define STEPPER_Y_LIMIT_OUT           P1OUT
#define STEPPER_Y_LIMIT_IN            P1IN

#define STEPPER_Z_LIMIT_PIN           BIT0
#define STEPPER_Z_LIMIT_DIR           P1DIR
#define STEPPER_Z_LIMIT_OUT           P1OUT
#define STEPPER_Z_LIMIT_IN            P1IN

//----------------------------------------------------------------------------
//--------------------------------- Structure --------------------------------
//----------------------------------------------------------------------------

typedef struct stepper_storage_t stepper_storage_t;
struct stepper_storage_t
{
    unsigned long x_step_mm;
    unsigned long y_step_mm;
};

typedef struct stepper_move_t stepper_move_t;
struct stepper_move_t
{
    long x;
    long y;
    long z;
    long f;
};

typedef struct stepper_buffer_t stepper_buffer_t;
struct stepper_buffer_t
{
    long x;
    long y;

    unsigned char x_dir;
    unsigned char y_dir;

    unsigned int x_feed;
    unsigned int y_feed;
};

typedef struct stepper_control_t stepper_control_t;
struct stepper_control_t
{
    unsigned char flags;

    long x_step_mm;
    long x_location;
    unsigned int x_step_count;

    long y_step_mm;
    long y_location;
    unsigned int y_step_count;

};

//----------------------------------------------------------------------------
//---------------------------------- General ---------------------------------
//----------------------------------------------------------------------------

void Stepper_Init (stepper_control_t *stcp);

//----------------------------------------------------------------------------
//-------------------------------- Interfaces --------------------------------
//----------------------------------------------------------------------------

void Stepper_Convert(stepper_control_t *stcp, stepper_buffer_t *xyz, stepper_move_t move);
void Stepper_Move(stepper_control_t *stcp, stepper_buffer_t *buf);
unsigned char Stepper_Available(stepper_control_t *stcp);
void Stepper_DisableMotors (stepper_control_t *stcp);
void Stepper_StopMotors (stepper_control_t *stcp);
unsigned char Stepper_LimitFlags (stepper_control_t *stcp, unsigned char mask);

#endif /* LIB_STEPPER_H_ */
