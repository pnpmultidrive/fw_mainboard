/*
 * timer.h
 *
 *  Created on: 25 ago. 2022
 *      Author: kamilo
 */

#ifndef LIB_TIMER_H_
#define LIB_TIMER_H_

#define TIMER_SIZE      3

#define TIMER_RS485     0
#define TIMER_MOTOR     1
#define TIMER_GCODE     2

#define Timer_Restart(t,c) (t->timer[c].count=0)

#define TIMER_DISCONTINUOUS 0
#define TIMER_CONTINUOUS    1
#define TIMER_B_ENABLE      0x01
#define TIMER_B_FINISH      0x02
#define TIMER_B_CONTINOUS   0x04

typedef struct to_t to_t;
struct to_t
{
    unsigned char flags;
    unsigned int count;
    unsigned int limit;
};

typedef struct timer_control_t timer_control_t;
struct timer_control_t
{
    to_t timer[TIMER_SIZE];
    unsigned char tick;
};

void Timer_Init (timer_control_t *tcp);
void Timer_Task (timer_control_t *tcp);

void Timer_Start(timer_control_t *tcp, unsigned char id, unsigned int time, unsigned char continuous);
unsigned char Timer_Available(timer_control_t *tcp, unsigned char id);
void Timer_Stop(timer_control_t *tcp, unsigned char id);



#endif /* LIB_TIMER_H_ */
